[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)

# SevDesk Integration for Flow Framework

This [Flow](https://flow.neos.io/) package allows you to integrate your Flow or Neos based application with [SevDesk](https://sevdesk.de/).
It doesn't cover all the possibilities of the SevDesk API but includes features to create Customers and Contacts as well as Invoices.
The package brings it's own models to simplify the creation of these records since the SevDesk API has some issues currently that are hidden when using the package.
The SevDesk API documentation can be found here: https://my.sevdesk.de/swaggerUI/index.html
Please note that this package still has some rough edges, e.g. the models don't validate the input so it's just forwared to the SevDesk API and relies on their validation (which should be changed in the future of course ;-)).

## Installation

To install the package just do a

```bash
$ composer require passcreator/sevdesk
```

## Configuration

To set up the package, add the following to your Settings.yaml

```yaml
SevDesk:
  apiToken: '1234567897564351453'
  sevUserId: '12345'
```

To get the API token, log in to SevDesk, go to Settings and Users and click on the user that you want to use to connect to the API.
The API token will be displayed at the bottom of that page.

To get the User ID, run the command ./flow sevdesk:users that is part of this package and copy the ID of your user and add it to the configuration.

### Creating a company

Use the Company Model of this package to provide company data to the sevDesk service.
Note that the SevDesk API expects the internal ID of the country of their system. Therefore there's a function called getIdByIsoCode that returns the SevDesk country ID when given the ISO code.
The list of countries in the SevDesk API is not correct which is the reason this list might be subject to change (e.g. Canada is duplicated, GB, UK and England are different countries etc...).

```php
    /**
     * @param Customer $customer
     */
    public function createCustomer(Customer $customer)
    {
        $company = new Company();
        $company->setName($customer->getName());
        $company->setName2('');
        $company->setStreet1($customer->getAddressStreet1());
        $company->setStreet2($customer->getAddressStreet2());
        $company->setZipCode($customer->getZipCode());
        $company->setCity($customer->getCity());
        $company->setVatNumber($customer->getVatRegistrationNo());

        $company->setEmailAddress($customer->getEmailAddress());

        $sevDeskCountryId = $this->sevDeskCountry->getIdByIsoCode('DE');

        $company->setCountry($sevDeskCountryId);
        $company->setCategory(Company::CATEGORY_CUSTOMER);

        // create the company contact in SevDesk
        $companyId = $this->sevDeskService->createCompany($company);
    }
```

### Creating a contact

Similar to creating a company.
Person is a Model of this package.

```php
    /**
     * @param Contact $contact
     */
    public function createCustomer(Contact $contact)
    {
        $person = new Person();
        $person->setFirstName($contact->getFirstName());
        $person->setFamilyName($contact->getLastName());
        $person->setParentCompany($contact->getSevDeskCustomerId());
        $this->sevDeskService->createPerson($person);
    }
```

## Creating an invoice

An invoice always consists of the invoice itself and invoice positions that represent the lines of the invoice.
Use the Invoice and InvoicePosition models of this package.

```php
        $invoice = new Invoice();
        $invoice->setContactId($customer->getSevDeskCustomerId());
        $invoice->setInvoiceDate($invoiceHeader->getInvoiceDate());
        $invoice->setDiscountTime(0);

        $taxRate = 19;

        if ($customer->getCountry() === 'DE') {
            $invoice->setTaxRate(19);
            $invoice->setTaxText("Umsatzsteuer ausweisen");
            $invoice->setTaxType(Invoice::TAX_TYPE_DEFAULT);
        } else {
            $invoice->setTaxRate(0);
            $taxRate = 0;
            $invoice->setTaxText("Steuerschuldnerschaft des Leistungsempfängers (Außerhalb EU, z.B. Schweiz)");
            $invoice->setTaxType(Invoice::TAX_TYPE_NOT_EU);
        }

        $invoice->setSmallSettlement(0);
        $invoice->setCurrencyCode("EUR");
        $invoice->setDiscount(0);
        $invoice->setStatus(Invoice::STATUS_CREATED);

        $invoice->setAddress($customer->getName() . "\n" . $customer->getFirstName() . ' ' . $customer->getLastName() . "\n" . $customer->getAddressStreet1() . "\n" . $customer->getZipCode() . ' ' . $customer->getCity());

        $invoice = $this->createInvoicePositions($invoice, $invoiceHeader, $taxRate);

        foreach ($invoiceHeader->getInvoiceLines() as $invoiceLine) {
            $invoicePosition = new InvoicePosition();
            $invoicePosition->setTaxRate($taxRate);
            $invoicePosition->setName($invoiceLine->getItemName());
            $invoicePosition->setPrice($invoiceLine->getPrice());
            $invoicePosition->setQuantity(1);
            $invoicePosition->setUnityId(InvoicePosition::UNITY_BLANKET);

            $invoice->addInvoicePosition($invoicePosition);
        }

        $this->sevDeskService->createInvoice($invoice);
```