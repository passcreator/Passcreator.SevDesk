<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 29.09.17
 * Time: 10:24
 */

namespace Passcreator\SevDesk\Command;

use Passcreator\SevDesk\Service\SevDeskService;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Cli\CommandController;

/**
 * @Flow\Scope("singleton")
 */
class SevDeskCommandController extends CommandController
{

    /**
     * @Flow\Inject
     * @var SevDeskService
     */
    protected $sevDeskService;

    /**
     * Returns information about the SevDesk user that is used to connect to the SevDesk API
     */
    public function usersCommand() {
        $this->output(\Neos\Flow\var_dump($this->sevDeskService->getUserInformation()[0]));
    }

    /**
     * Returns information about contacts in SevDesk
     */
    public function contactsCommand() {
        foreach ($this->sevDeskService->getContactInformation() as $contact) {
            $this->outputLine('Contact name: '.$contact->name.' - SevDesk ID: '.$contact->id);
        }
    }

}