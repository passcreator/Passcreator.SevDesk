<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 03.09.17
 * Time: 09:51
 */

namespace Passcreator\SevDesk\Model;


class Country
{
    const GERMANY = 1;
    const SWITZERLAND = 2;
    const AUSTRIA = 3;
    const AFGHANISTAN = 4;
    const EGYPT = 1383;
    const ALBANIA = 1347;
    const ANDORRA = 1418;
    const ANTIGUA_AND_BARBUDA = 1418;
    const ARGENTINIA = 5;
    const ARMENIA = 1348;
    const AZERBAIJAN = 33;
    const ETHIOPIA = 1399;
    const AUSTRALIA = 38;
    const BAHAMAS = 1426;
    const BAHRAIN = 1427;
    const BANGLADESH = 1440;
    const BARBADOS = 1445;
    const BELGIUM = 6;
    const BELIZE = 46;
    const BENIN = 1437;
    const BHUTAN = 1350;
    const BOSNIA = 53;
    const BOSNIA_HERZEGOVINA = 1396;
    const BRAZIL = 73;
    const VIRGIN_ISLANDS = 1391;
    const BULGARIA = 7;
    const BURUNDI = 1404;
    const CHILE = 49;
    const CHINA = 60;
    const COSTA_RICA = 1421;
    const DENMARK = 8;
    const DOMINICAN_REPUBLIC = 1447;
    const DJBOUTI = 1405;
    const DUBAI = 67;
    const ECUADOR = 1434;
    const EL_SALVADOR = 1450;
    const IVORY_COAST = 1381;
    const ENGLAND = 74;
    const ERITREA = 1406;
    const ESTONIA = 72;
    const FINLAND = 10;
    const FRANCE = 11;
    const FRENCH_POLYNESIA = 1351;
    const GEORGIA = 1394;
    const GHANA = 1382;
    const GIBRALTAR = 1377;
    const GRENADA = 1402;
    const GREECE = 12;
    const UNITED_KINGDOM = 9;
    const GUADELOUPE = 1438;
    const GUINEA = 1376;
    const HONG_KONG = 59;
    const INDENE = 62;
    const INDIA = 1375;
    const INDONESIA = 1374;
    const IRAQ = 1419;
    const IRAN = 1373;
    const IRELAND = 13;
    const ICELAND = 51;
    const ISRAEL = 36;
    const ITALY = 14;
    const JAMAICA = 15;
    const JAPAN = 40;
    const JERSEY = 1441;
    const JORDAN = 1344;
    const CAMBODIA = 1401;
    const CAMEROON = 1400;
    const CANADA = 39;
    //const CANADA = 1423;
    const KAZAKHSTAN = 56;
    const QATAR = 1371;
    const KENYA = 1389;
    const KYRGYZSTAN = 1430;
    const COLOMBIA = 1403;
    const COMOROS = 1407;
    const KOSOVO = 1397;
    //const KOSOVO = 1422;
    const CROATIA = 48;
    const KUWAIT = 1369;
    const LATVIA = 16;
    const LEBANON = 1428;
    const LIECHTENSTEIN = 42;
    const LITHUANIA = 1346;
    const LUXEMBOURG = 17;
    const MACAO = 61;
    const MADAGASKAR = 1395;
    const MALAWI = 1408;
    const MALAYSIA = 1387;
    const MALEDIVES = 1435;
    const MALTA = 37;
    const MOROCCO = 1365;
    const MARTINIQUE = 1385;
    const MAURITANIA = 1364;
    const MAURITIUS = 1363;
    const MAYOTTE = 1409;
    const MACEDONIA = 1367;
    const MEXICO = 64;
    const MOLDOVA = 1362;
    const MONACO = 43;

    protected $isoCodeIdMapping = array(
        "DE" => Country::GERMANY,
        "CH" => Country::SWITZERLAND,
        "AT" => Country::AUSTRIA,
        "AF" => Country::AFGHANISTAN,
        "EG" => Country::EGYPT,
        "AL" => Country::ALBANIA,
        "AD" => Country::ANDORRA,
        "AG" => Country::ANTIGUA_AND_BARBUDA,
        "AR" => Country::ARGENTINIA,
        "AM" => Country::ARMENIA,
        "AZ" => Country::AZERBAIJAN,
        "ET" => Country::ETHIOPIA,
        "AU" => Country::AUSTRALIA,
        "BS" => Country::BAHAMAS,
        "BH" => Country::BAHRAIN,
        "BD" => Country::BANGLADESH,
        "BB" => Country::BARBADOS,
        "BE" => Country::BELGIUM,
        "BZ" => Country::BELIZE,
        "BJ" => Country::BENIN,
        "BT" => Country::BHUTAN,
        "BA" => Country::BOSNIA_HERZEGOVINA,
        "BR" => Country::BRAZIL,
        "VG" => Country::VIRGIN_ISLANDS,
        "BG" => Country::BULGARIA,
        "BI" => Country::BURUNDI,
        "CL" => Country::CHILE,
        "CN" => Country::CHINA,
        "CR" => Country::COSTA_RICA,
        "DK" => Country::DENMARK,
        "DO" => Country::DOMINICAN_REPUBLIC,
        "DJ" => Country::DJBOUTI,
        //"DUBAI" => Country::DUBAI,
        "EC" => Country::ECUADOR,
        "SV" => Country::EL_SALVADOR,
        "CI" => Country::IVORY_COAST,
        //"ENGLAND" => Country::ENGLAND,
        "ER" => Country::ERITREA,
        "EE" => Country::ESTONIA,
        "FI" => Country::FINLAND,
        "FR" => Country::FRANCE,
        "PF" => Country::FRENCH_POLYNESIA,
        "GE" => Country::GEORGIA,
        "GH" => Country::GHANA,
        "GI" => Country::GIBRALTAR,
        "GD" => Country::GRENADA,
        "GR" => Country::GREECE,
        "GB" => Country::UNITED_KINGDOM,
        "GP" => Country::GUADELOUPE,
        "GN" => Country::GUINEA,
        "HK" => Country::HONG_KONG,
        //"INDENE" => Country::INDENE,
        "IN" => Country::INDIA,
        "ID" => Country::INDONESIA,
        "IQ" => Country::IRAQ,
        "IR" => Country::IRAN,
        "IE" => Country::IRELAND,
        "IS" => Country::ICELAND,
        "IL" => Country::ISRAEL,
        "IT" => Country::ITALY,
        "JM" => Country::JAMAICA,
        "JP" => Country::JAPAN,
        //"JERSEY" => Country::JERSEY,
        "JO" => Country::JORDAN,
        "KH" => Country::CAMBODIA,
        "CM" => Country::CAMEROON,
        "CA" => Country::CANADA,
        "KZ" => Country::KAZAKHSTAN,
        "QA" => Country::QATAR,
        "KE" => Country::KENYA,
        "KG" => Country::KYRGYZSTAN,
        "CO" => Country::COLOMBIA,
        "KM" => Country::COMOROS,
        //"KOSOVO" => Country::KOSOVO,
        "HR" => Country::CROATIA,
        "KW" => Country::KUWAIT,
        "LV" => Country::LATVIA,
        "LB" => Country::LEBANON,
        "LI" => Country::LIECHTENSTEIN,
        "LT" => Country::LITHUANIA,
        "LU" => Country::LUXEMBOURG,
        "MO" => Country::MACAO,
        "MG" => Country::MADAGASKAR,
        "MW" => Country::MALAWI,
        "MY" => Country::MALAYSIA,
        "MV" => Country::MALEDIVES,
        "MT" => Country::MALTA,
        "MA" => Country::MOROCCO,
        "MQ" => Country::MARTINIQUE,
        "MR" => Country::MAURITANIA,
        "MU" => Country::MAURITIUS,
        "YT" => Country::MAYOTTE,
        "MK" => Country::MACEDONIA,
        "MX" => Country::MEXICO,
        "MD" => Country::MOLDOVA,
        "MC" => Country::MONACO
    );

    /**
     * @param $isoCode
     * @return int
     */
    public function getIdByIsoCode($isoCode) {
        if(isset($this->isoCodeIdMapping[$isoCode])) {
            return $this->isoCodeIdMapping[$isoCode];
        }

        return null;
    }
}