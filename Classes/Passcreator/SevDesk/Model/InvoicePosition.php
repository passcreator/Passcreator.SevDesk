<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 03.09.17
 * Time: 12:43
 */

namespace Passcreator\SevDesk\Model;


class InvoicePosition
{

    const UNITY_PIECE = 1;
    const UNITY_SQUARE_METER = 2;
    const UNITY_METER = 3;
    const UNITY_KILOGRAM = 4;
    const UNITY_TON = 5;
    const UNITY_RUNNING_METER = 6;
    const UNITY_BLANKET = 7;
    const UNITY_CUBIC_METER = 8;
    const UNITY_HOUR = 9;
    const UNITY_KILOMETER = 10;
    const UNITY_PERCENT = 11;
    const UNITY_DAYS = 13;
    const UNITY_L = 15;
    const UNITY_EMTPY = 37;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var double
     */
    protected $price;

    /**
     * UnityId 1 means Stk
     * @var int
     */
    protected $unityId = 1;

    /**
     * Needed together with unityId (means Stk in german)
     * @var string
     */
    protected $unityObjectName = "Unity";

    /**
     * The tax rate of this invoice position
     * @var int
     */
    protected $taxRate;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getUnityId()
    {
        return $this->unityId;
    }

    /**
     * @param int $unityId
     */
    public function setUnityId($unityId)
    {
        $this->unityId = $unityId;
    }

    /**
     * @return string
     */
    public function getUnityObjectName()
    {
        return $this->unityObjectName;
    }

    /**
     * @return int
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * @param int $taxRate
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;
    }
}