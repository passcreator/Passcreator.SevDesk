<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 01.09.17
 * Time: 07:17
 */

namespace Passcreator\SevDesk\Model;


class Invoice
{

    const TAX_TYPE_DEFAULT = 'default';
    const TAX_TYPE_EU = 'eu';
    const TAX_TYPE_NOT_EU = 'noteu';

    const STATUS_CREATED = 100;
    const STATUS_DELIVERED = 200;
    const STATUS_PAID = 1000;

    /**
     * @var string
     */
    protected $invoiceNumber;

    /**
     * @var string
     */
    protected $invoiceType = 'RE';

    /**
     * @var \DateTime
     */
    protected $invoiceDate;

    /**
     * @var \DateTime
     */
    protected $deliveryDate;

    /**
     * @var \DateTime
     */
    protected $deliveryDateUntil;

    /**
     * @var string
     */
    protected $contactId;

    /**
     * @var string
     */
    protected $contactObjectName = 'Contact';

    /**
     * @var int
     */
    protected $discountTime = 0;

    /**
     * @var int
     */
    protected $taxRate;

    /**
     * @var int
     */
    protected $timeToPay = 7;

    /**
     * @var string
     */
    protected $taxText;

    /**
     * @var string
     */
    protected $taxType = Invoice::TAX_TYPE_DEFAULT;

    /**
     * @var int
     */
    protected $smallSettlement;

    /**
     * @var string
     */
    protected $currencyCode = 'EUR';

    /**
     * @var int
     */
    protected $discount = 0;

    /**
     * @var int
     */
    protected $status = Invoice::STATUS_CREATED;

    /**
     * @var string
     */
    protected $address;

    /**
     * An array of invoice position objects
     * @var array()
     */
    protected $invoicePositions = array();

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    }

    /**
     * @return string
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * @param string $invoiceType
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;
    }

    /**
     * @return \DateTime
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * @param \DateTime $invoiceDate
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDateUntil()
    {
        return $this->deliveryDateUntil;
    }

    /**
     * @param \DateTime $deliveryDateUntil
     */
    public function setDeliveryDateUntil($deliveryDateUntil)
    {
        $this->deliveryDateUntil = $deliveryDateUntil;
    }

    /**
     * @return string
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * @param string $contactId
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;
    }

    /**
     * @return string
     */
    public function getContactObjectName()
    {
        return $this->contactObjectName;
    }

    /**
     * @return int
     */
    public function getDiscountTime()
    {
        return $this->discountTime;
    }

    /**
     * @param int $discountTime
     */
    public function setDiscountTime($discountTime)
    {
        $this->discountTime = $discountTime;
    }

    /**
     * @return int
     */
    public function getTaxRate()
    {
        return $this->taxRate;
    }

    /**
     * @param int $taxRate
     */
    public function setTaxRate($taxRate)
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return int
     */
    public function getTimeToPay()
    {
        return $this->timeToPay;
    }

    /**
     * @param int $timeToPay
     */
    public function setTimeToPay($timeToPay)
    {
        $this->timeToPay = $timeToPay;
    }

    /**
     * @return string
     */
    public function getTaxText()
    {
        return $this->taxText;
    }

    /**
     * @param string $taxText
     */
    public function setTaxText($taxText)
    {
        $this->taxText = $taxText;
    }

    /**
     * @return string
     */
    public function getTaxType()
    {
        return $this->taxType;
    }

    /**
     * @param string $taxType
     */
    public function setTaxType($taxType)
    {
        $this->taxType = $taxType;
    }

    /**
     * @return int
     */
    public function getSmallSettlement()
    {
        return $this->smallSettlement;
    }

    /**
     * @param int $smallSettlement
     */
    public function setSmallSettlement($smallSettlement)
    {
        $this->smallSettlement = $smallSettlement;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return array
     */
    public function getInvoicePositions()
    {
        return $this->invoicePositions;
    }

    /**
     * @param array $invoicePositions
     */
    public function setInvoicePositions($invoicePositions)
    {
        $this->invoicePositions = $invoicePositions;
    }

    /**
     * @param InvoicePosition $invoicePosition
     */
    public function addInvoicePosition(InvoicePosition $invoicePosition) {
        $this->invoicePositions[] = $invoicePosition;
    }

}