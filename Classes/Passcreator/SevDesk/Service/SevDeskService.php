<?php
/**
 * Created by PhpStorm.
 * User: davidsporer
 * Date: 01.09.17
 * Time: 07:15
 */

namespace Passcreator\SevDesk\Service;


use Passcreator\SevDesk\Model\Company;
use Passcreator\SevDesk\Model\Invoice;
use Passcreator\SevDesk\Model\InvoicePosition;
use Passcreator\SevDesk\Model\Person;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Exception;

class SevDeskService
{

    /**
     * @Flow\InjectConfiguration(package="Passcreator.SevDesk")
     * @var array
     */
    protected $settings;

    /**
     * @param Company $company
     * @return int
     */
    public function createCompany(Company $company)
    {
        $company->setCustomerNumber($this->getNextCustomerNumber());

        $data = array(
            'objectName' => 'Contact',
            'name' => $company->getName(),
            'name2' => $company->getName2(),
            'update' => 0,
            'create' => 0,
            'customerNumber' => $company->getCustomerNumber(),
            'vatNumber' => $company->getVatNumber(),
            'bankAccount' => $company->getBankAccount(),
            'bankNumber' => $company->getBankNumber(),
            'category' => array(
                'objectName' => 'Category',
                'id' => $company->getCategory()
            )
        );

        $response = $this->callApi($this->settings['apiEndpoints']['contact'], $data, true);

        $customerNumber = $response->objects->id;

        $dataAddress = array(
            'street' => $company->getStreet1(),
            'zip' => $company->getZipCode(),
            'city' => $company->getCity(),
            'country' => $company->getCountry(),
            'category' => Company::ADDRESS_WORK
        );

        $this->callApi(sprintf($this->settings['apiEndpoints']['address'], $customerNumber), $dataAddress,
            true);

        $dataEmail = array(
            'key' => 2,
            'value' => $company->getEmailAddress()
        );

        $this->callApi(sprintf($this->settings['apiEndpoints']['email'], $customerNumber), $dataEmail,
            true);

        return $customerNumber;
    }

    /**
     * @param Person $person
     */
    public function createPerson(Person $person)
    {

        $person->setCustomerNumber($this->getNextCustomerNumber());

        $data = array(
            'objectName' => 'Contact',
            'surename' => $person->getFirstName(),
            'familyname' => $person->getFamilyName(),
            'category' => array(
                'id' => 3,
                'objectName' => 'Category'
            ),
            'parent' => array(
                'id' => $person->getParentCompany(),
                'objectName' => 'Contact'
            ),
            'customerNumber' => $person->getCustomerNumber()
        );

        $this->callApi($this->settings['apiEndpoints']['contact'], $data, true);
    }

    public function createInvoice(Invoice $invoice)
    {
        $nextInvoiceNumber = $this->getNextInvoiceNumber();

        $data = array(
            'header' => 'Invoice ' . $nextInvoiceNumber,
            'invoiceNumber' => $nextInvoiceNumber,
            'invoiceType' => 'RE',
            'invoiceDate' => $invoice->getInvoiceDate()->format(\DateTime::ISO8601),
            'deliveryDate' => $invoice->getDeliveryDate()->format(\DateTime::ISO8601),
            'deliveryDateUntil' => $invoice->getDeliveryDateUntil()->format(\DateTime::ISO8601),
            'contactPerson' => array(
                'id' => $this->settings['sevUserId'],
                'objectName' => 'SevUser'
            ),
            'contact' => array(
                'objectName' => 'Contact',
                'id' => $invoice->getContactId()
            ),
            'discountTime' => $invoice->getDiscountTime(),
            'taxRate' => $invoice->getTaxRate(),
            'timeToPay' => $invoice->getTimeToPay(),
            'taxText' => $invoice->getTaxText(),
            'taxType' => $invoice->getTaxType(),
            'smallSettlement' => $invoice->getSmallSettlement(),
            'currency' => $invoice->getCurrencyCode(),
            'discount' => $invoice->getDiscount(),
            'status' => $invoice->getStatus(),
            'address' => $invoice->getAddress()
        );

        $response = $this->callApi($this->settings['apiEndpoints']['invoice'], $data, true);

        if($response->objects === null) {
            throw new Exception($response->error->message, 1510124169);
        } else {
            $invoiceId = $response->objects->id;

            $this->createInvoicePositions($invoice, $invoiceId);
        }
    }

    /**
     * @param Invoice $invoice
     * @param $invoiceId
     */
    protected function createInvoicePositions(Invoice $invoice, $invoiceId)
    {
        foreach ($invoice->getInvoicePositions() as $invoicePosition) {
            $data = array(
                'invoice' => array(
                    'id' => $invoiceId,
                    'objectName' => 'Invoice'
                ),
                'name' => $invoicePosition->getName(),
                'quantity' => $invoicePosition->getQuantity(),
                'price' => $invoicePosition->getPrice(),
                'unity' => array(
                    'id' => $invoicePosition->getUnityId(),
                    'objectName' => $invoicePosition->getUnityObjectName()
                ),
                'taxRate' => $invoicePosition->getTaxRate()
            );

            $this->callApi($this->settings['apiEndpoints']['invoicePosition'], $data, true);
        }
    }

    /**
     * get's the info about the current SevDesk user and returns it
     * @return array
     */
    public function getUserInformation()
    {
        $response = $this->callApi($this->settings['apiEndpoints']['userInformation'], array());
        return $response->objects;
    }

    /**
     * get's the info about the Contacts in sevDesk
     * @return array
     */
    public function getContactInformation()
    {
        $response = $this->callApi($this->settings['apiEndpoints']['contact'], array());
        return $response->objects;
    }

    /**
     * Gets the next customer number using the API and returns it
     * @return mixed
     */
    protected function getNextCustomerNumber()
    {
        $response = $this->callApi($this->settings['apiEndpoints']['nextCustomerNumber'], array());
        return $response->objects;
    }

    /**
     * Gets the next invoice number using the API and returns it
     * @return mixed
     */
    protected function getNextInvoiceNumber()
    {
        $response = $this->callApi($this->settings['apiEndpoints']['nextInvoiceNumber'], array());
        return $response->objects;
    }

    /**
     * @param string $endpointUrl
     * @param array $data
     * @param bool $isPost
     * @return array
     */
    protected function callApi($endpointUrl, $data = array(), $isPost = false)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $endpointUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        if ($isPost) {
            curl_setopt($ch, CURLOPT_POST, true);
        }

        if ($isPost && !empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: " . $this->settings['apiToken'],
            "Content-Type: application/x-www-form-urlencoded"
        ));

        $response = \json_decode(curl_exec($ch));

        return $response;
    }

}